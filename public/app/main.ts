import { bootstrap } from '@angular/platform-browser-dynamic';
import { AppComponent } from './src/components/app/app.component';

bootstrap(AppComponent);
