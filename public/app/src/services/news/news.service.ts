import {Http, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class NewsService {
  constructor(private http:Http){
  }
  getNews() {
    return this.http.get('/api/news')
      .map((res: Response) => res.json());
  }
}
