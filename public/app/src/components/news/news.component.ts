import { Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';

import { NewsService } from '../../services/news/news.service';

@Component({
    selector: 'news-list',
    providers: [NewsService, HTTP_PROVIDERS],
    templateUrl: './app/src/components/news/news.component.html'
})
export class NewsComponent {
    public news = <Object>[];

    constructor(private newsService: NewsService) {
        this.loadNews();
    }
    loadNews() {
        this.newsService
            .getNews()
            .subscribe((news: Array<Object>) => this.news = news);
    }
    stringAsDate(dateStr: string){
      return new Date(dateStr);
    }
}
