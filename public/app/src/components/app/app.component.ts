import { Component } from '@angular/core';
import { NewsComponent } from '../news/news.component';

@Component({
  selector: 'app',
  directives: [NewsComponent],
  templateUrl: './app/src/components/app/app.component.html'
})
export class AppComponent {}
