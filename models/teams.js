var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Users = require('./users');

var TeamsSchema = new Schema({
  name: { type: String, required: true },
  short: { type: String },
  createdAt: { type: Date, default: Date.now },
  ownedBy: { type: Schema.types.ObjectId, ref: 'Users', required: true}
});

module.exports = mongoose.model('Teams', TeamsSchema);
