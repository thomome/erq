var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var Schema = mongoose.Schema;

var UsersSchema = new Schema({
  username: { type: String, unique: true, required: true },
  email: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  permissions: { type: [String], required: true },
  name: { type: String },
  introduction: { type: String },
  createdAt: { type: Date, default: Date.now }
});

UsersSchema.pre('save', function(callback) {
  var users = this;

  if(!users.isModified('password')) return callback();

  bcrypt.genSalt(5, function(err, salt) {
    if(err) return callback(err);

    bcrypt.hash(users.password, salt, null, function(err, hash) {
      if(err) return callback(err);
      users.password = hash;
      callback();
    })
  })
});

UsersSchema.methods.verifyPassword = function(password, cb) {
  bcrypt.compare(password, this.password, function(err, isMatch) {
    if(err) return cb(err);
    cb(null, isMatch);
  });
};

module.exports = mongoose.model('Users', UsersSchema);
