var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Games = require('./games');
var Teams = require('./teams');
var Users = require('./users');

var AccountsSchema = new Schema({
  name: { type: String, required: true },
  game: { Schema.types.ObejctId, ref: 'Games', required: true },
  team: { Schema.types.ObjectId, ref: 'Teams', required: true },
  ownedBy: { Schema.types.ObjectId, ref: 'Users', required: true },
  customData: { type: Schema.types.Mixed },
  createdAt: {type: Date, default: Date.now }
});

module.exports = mongoose.model('Accounts', AccountsSchema);
