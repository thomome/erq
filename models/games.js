var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GamesSchema = new Schema({
  name: { type: String, required: true },
  short: { type: String },
  link: {type: String }
});

module.exports = mongoose.model('Games', GamesSchema);
