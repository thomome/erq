var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Users = require('./users.js');

var NewsSchema = new Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  active: { type: Boolean, default: true },
  createdBy: { type: Schema.Types.ObjectId, ref: 'Users', required: true },
  createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('News', NewsSchema);
