var express = require('express');
var router = express.Router();

var authController = require('../controllers/auth');

// GET home page
router.get('/', function(req, res){
  res.json({message: 'Welcome to the ERQ API'});
});

module.exports = router;
