var express = require('express');
var router = express.Router();

var newsController = require('../controllers/news');
var authController = require('../controllers/auth');

// GET /news listing
router.route('/')
  .post(authController.authenticate(), newsController.postNews)
  .get(newsController.getNews);

router.route('/:news_id')
  .get(newsController.getNewsById)
  .put( newsController.putNews)
  .delete(newsController.deleteNews);

module.exports = router;
