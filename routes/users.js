var express = require('express');
var router = express.Router();

var usersController = require('../controllers/users.js');

router.route('/')
  .post(usersController.postUsers)
  .get(usersController.getUsers);

module.exports = router;
