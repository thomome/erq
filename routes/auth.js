var express = require('express');
var router = express.Router();

var authController = require('../controllers/auth');

router.route('/login').post(authController.postLogin);

module.exports = router;
