var Users = require('../models/users');

exports.postUsers = function(req, res) {
  var users = new Users();

  users.username = req.body.username;
  users.password = req.body.password;
  users.email = req.body.email;

  users.save(function(err) {
    if(err)
      res.send(err);

    res.json({ message: 'User added' });
  });
};

exports.getUsers = function(req, res) {
  Users.find(function(err, users) {
    if(err)
      res.send(err);

    res.json(users);
  });
};
