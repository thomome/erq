var passport = require('passport');
var passportJwt = require('passport-jwt');
var jwt = require('jwt-simple');
var ExtractJwt = passportJwt.ExtractJwt;
var JwtStrategy = passportJwt.Strategy;

var cfg = require('../config');
var Users = require('../models/users');

var params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJwt.fromAuthHeader()
};

passport.use(new JwtStrategy(params, function(payload, callback){
  if(new Date(payload.expiration) > Date.now()){
    Users.findById(payload.id, function(err, users){
      if(err) { return callback(err); }

      if(!users) { callback(null, false); }

      if(users) { callback(null, users); }

    });
  } else {
    callback(null, false);
  }
}));

exports.initialize =  function(){
  return passport.initialize();
};
exports.authenticate = function(){
  return passport.authenticate('jwt', cfg.jwtSession);
};

exports.postLogin = function(req, res) {
  if(req.body.username && req.body.password){
    var username = req.body.username;
    var password = req.body.password;

    Users.findOne({ username: username },function(err, users){
      if(err) {
         res.sendStatus(401);
      } else if(users){
        users.verifyPassword(password, function(err, isMatch) {
          if(err) {
            res.sendStatus(401);
          } else {
            if(isMatch){
              var expiration = new Date();
              expiration.setDate(expiration.getDate() + 7);
              var payload = { id: users.id, expiration: expiration };
              var token = jwt.encode(payload, cfg.jwtSecret);
              res.json({ token: token });
            } else {
              res.sendStatus(401);
            }
          }
        });
      } else {
        res.sendStatus(401);
      }
    });
  } else {
    res.sendStatus(401);
  }
};

exports.hasPermissions = function(permission){
  return function(req,res,next){

    if(req.user.permissions.indexOf(permission) !== -1){
      next();
    } else {
      var err = new Error();
      err.status = 403;
      err.message = 'Forbidden';
      next(err);
    }
  };
};
