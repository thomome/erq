var News = require('../models/news');

exports.postNews = function(req, res) {
  var news = new News();

  news.title = req.body.title;
  news.content = req.body.content;
  news.active = req.body.active;
  news.createdBy = req.user._id;

  news.save(function(err) {
    if(err)
      res.send(err);

    res.json({message: 'News added', data: news });
  });
};

exports.getNews = function(req,res) {
  News.find().populate('createdBy', 'username createdAt').exec(function(err, news) {
    if(err)
      res.send(err);

    res.json(news);
  });
}

exports.getNewsById = function(req, res) {
  News.findById(req.params.news_id).populate('createdBy', 'username createdAt').exec(function(err, news) {
    if(err)
      res.send(err);

    res.json(news);
  });
};

exports.putNews = function(req, res) {
  News.update({
    _id: req.params.news_id
  }, {
    title: req.body.title,
    content: req.body.content
  }, function(err, num, raw){
    if(err)
      res.send(err);

    res.json({ message: req.params.news_id + ' updated' });
  });
};

exports.deleteNews = function(req, res) {
  News.findByIdAndRemove(req.params.news_id, function(err) {
    if(err)
      res.send(err);

    res.json({ message: 'News removed' });
  });
};
