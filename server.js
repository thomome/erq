var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');

var authController = require('./controllers/auth');

var index = require('./routes/index');
var api = require('./routes/api');
var auth = require('./routes/auth');
var news = require('./routes/news');
var users = require('./routes/users');

mongoose.connect('mongodb://localhost/erq', function(err){
  if(err){
    console.log('connection error', err);
  } else {
    console.log('connection successful');
  }
});

var app = express();

//view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon(__dirname + '/public/app/favicon.png'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(require('connect-flash')());

app.use(authController.initialize());

app.use('/', index);
app.use('/api', api);
app.use('/api/auth', auth);
app.use('/api/news', news);
app.use('/api/users', users);

app.use(function(req, res, next){
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if(app.get('env') === 'development') {
  app.use(function(err, req, res, next){
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

app.use(function(err, req, res, next){
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
